
uses
  SysUtils, Play2Mid;

var
  LText, LLine: string;
  t: text;
  
begin
  if (ParamCount = 1) and FileExists(ParamStr(1)) then
  begin
    (* Read text file *)
    Assign(t, ParamStr(1));
    Reset(t);
    SetLength(LText, 0);
    while not Eof(t) do
    begin
      ReadLn(t, LLine);
      if (Length(LLine) > 0) and (LLine[1] <> ';') then
        LText := LText + LLine;
    end;
    Close(t);
    (* Set MIDI file name *)
    Play2Mid.midiFileName := ChangeFileExt(ParamStr(1), '.mid');
    (* Create MIDI file *)
    Play2Mid.Play(LText);
  end;
end.
